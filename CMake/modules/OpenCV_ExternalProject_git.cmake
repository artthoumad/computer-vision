ExternalProject_Add(
	OpenCV

	GIT_PROGRESS 1

	GIT_REPOSITORY "https://github.com/opencv/opencv.git"
	GIT_TAG "3.4.1"

	UPDATE_COMMAND ""
	
	# Fix bug due to deprecation in latest GNU toolset versions
	PATCH_COMMAND ${CMAKE_COMMAND} -E copy
		"${CMAKE_SOURCE_DIR}/CMake/patches/OpenCV/CMakeLists.txt" <SOURCE_DIR>/CMakeLists.txt

	SOURCE_DIR "${CMAKE_SOURCE_DIR}/thirdparty/OpenCV"
	CMAKE_ARGS -DBUILD_SHARED_LIBS=OFF -DCMAKE_INSTALL_PREFIX=${CMAKE_BINARY_DIR}/OpenCV

	TEST_COMMAND ""
)
