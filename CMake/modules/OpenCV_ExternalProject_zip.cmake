ExternalProject_Add(
	OpenCV

	URL https://github.com/opencv/opencv/archive/3.4.1.zip

	UPDATE_COMMAND ""
	PATCH_COMMAND ${CMAKE_COMMAND} -E copy
		"${CMAKE_SOURCE_DIR}/CMake/patches/OpenCV/CMakeLists.txt" <SOURCE_DIR>/CMakeLists.txt

	SOURCE_DIR "${CMAKE_SOURCE_DIR}/thirdparty/OpenCV"
	CMAKE_ARGS -DBUILD_SHARED_LIBS=OFF -DCMAKE_INSTALL_PREFIX=${CMAKE_BINARY_DIR}/OpenCV

	TEST_COMMAND ""
)
