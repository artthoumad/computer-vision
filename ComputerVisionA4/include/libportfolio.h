#include <opencv2/opencv.hpp>
#include <string>
#include <vector>

namespace libportfolio
{
	const int WEBCAM_FEED = 0;

	class Image
	{
	public:
		Image(const std::string& filename);
		Image(const cv::Mat& data);

		const cv::Mat& getData() const;
		int getHeight() const;
		int getWidth() const;

		Image pad(int padWidth, int r, int g, int b) const;
		Image hConcat(const Image& target) const;
		// Lab 6
		Image rgb2gs() const;
		Image logScale() const;
		Image meanFilter(int radius) const;
		Image medianFilter(int radius) const;
		Image gaussianFilter(int radius, double sigma) const;

		// Lab 7
		Image scharrEdgeDetection(double minValue) const;

		// Lab 8
		Image cannyEdgeDetection() const;

		// Lab 9
		Image stitch(const Image& target) const;
		void show() const;
	private:
		cv::Mat imageData;
		int width;
		int height;
	};

	class Video
	{
	public:
		Video(const std::string& filename);
		Video(int num);

		void play(double scaleFactor, std::function<Image(Image, int* trackBarValue)> frameCallback, bool trackBar);
		Image getFrame(int frame) const;
	private:
		double fps;
		int width;
		int height;
		cv::VideoCapture videoCapture;
	};

	class GaussianPyramid
	{
	public:
		static int nextPowerTwo(int i);

		GaussianPyramid(const Image& image, unsigned int levels);
		GaussianPyramid(const GaussianPyramid& pyramid);

		GaussianPyramid operator=(const GaussianPyramid& pyramid);

		void setImage(const Image& image, unsigned int levels);

		const cv::Mat& getLevel(unsigned int level) const;

		void display() const;
	private:
		std::vector<cv::Mat> pyramid;
	};
}
