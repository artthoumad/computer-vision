#include <opencv2/opencv.hpp>
#include <libportfolio.h>
#include <stdexcept>
#include <iostream>
#include <sstream>
#include <vector>
#include <math.h>

using namespace libportfolio;

Image::Image(const std::string& filename)
{
	imageData = cv::imread(filename, CV_LOAD_IMAGE_COLOR);
	if (!imageData.data)
	{
    	throw std::runtime_error(std::string("Unable to open ").append(filename));
	}
	height = imageData.rows;
	width = imageData.cols;
}

Image::Image(const cv::Mat& data)
{
	if(!data.data)
	{
    	throw std::runtime_error("Parameter image was empty.");
	}
	imageData = data;
	height = data.rows;
	width = data.cols;
}

const cv::Mat& Image::getData() const
{
	return imageData;
}

int Image::getWidth() const
{
	return width;
}

int Image::getHeight() const
{
	return height;
}

Image Image::pad(int padWidth, int r, int g, int b) const
{
	cv::Mat result(height + padWidth*2,
			width + padWidth*2,
			CV_8UC3,
			cv::Scalar(b, g, r));
	imageData.copyTo(result(cv::Rect(padWidth, padWidth, imageData.cols, imageData.rows)));
	return result;
}

Image Image::hConcat(const Image& target) const
{
	int maxHeight = (height > target.getHeight()) ? height : target.getHeight();
	cv::Mat result(maxHeight,
			width + target.getWidth(),
			CV_8UC3,
			cv::Scalar(128, 128, 128));
	imageData.copyTo(result(cv::Rect(0, 0, imageData.cols, imageData.rows)));
	target.getData().copyTo(result(cv::Rect(imageData.cols, 0, target.getWidth(), target.getHeight())));
	return result;
}

Image Image::rgb2gs() const
{
	cv::Mat gsData;
	cv::cvtColor(imageData, gsData, CV_RGB2GRAY);
	return Image(gsData);
}

Image Image::logScale() const
{
	cv::Mat floatData;
	cv::Mat logData;
	cv::Mat normLogData;

    rgb2gs().imageData.convertTo(floatData, CV_32FC1);
	cv::log(floatData + 1.0, logData);
	cv::normalize(logData, normLogData, 0, 255, cv::NORM_MINMAX, CV_8UC1);
	return Image(normLogData);
}

Image Image::meanFilter(int radius) const
{
	cv::Mat meanData;
	cv::Size filterSize(2*radius+1, 2*radius+1);
	cv::blur(imageData, meanData, filterSize);
	return Image(meanData);
}

Image Image::medianFilter(int radius) const
{
	cv::Mat medianData;
	cv::medianBlur(imageData, medianData, 2*radius+1);
	return Image(medianData);
}

Image Image::gaussianFilter(int radius, double sigma) const
{
	cv::Mat gaussianData;
	cv::Size filterSize(2*radius+1, 2*radius+1);
	cv::GaussianBlur(imageData, gaussianData, filterSize, sigma, sigma);
	return Image(gaussianData);
}

Image Image::scharrEdgeDetection(double minValue) const
{
	cv::Mat gsData = rgb2gs().imageData;
	cv::Mat floatData;
	cv::Mat normData;
	cv::Mat gaussianData;

	cv::Mat scharrXData;

	cv::Mat scharrYData;

	cv::Mat scharrCombine;
	cv::Mat scharrThresh;
	cv::Mat normData2;
	cv::Mat scharrReturn;

	gsData.convertTo(floatData, CV_32F);

	cv::normalize(floatData, normData, 0.0, 1.0, cv::NORM_MINMAX);

	cv::bilateralFilter(normData, gaussianData, 5, 20, 20);

	cv::normalize(floatData, normData, 0.0, 1.0);

	cv::Scharr(gaussianData, scharrXData, CV_32F, 1, 0); // X axis

	cv::Scharr(gaussianData, scharrYData, CV_32F, 0, 1); // Y axis

	scharrCombine = 0.5f*cv::abs(scharrXData) + 0.5f*cv::abs(scharrYData);
	cv::threshold(scharrCombine, scharrThresh, minValue, 1.0f, cv::THRESH_BINARY);
	cv::normalize(scharrThresh, normData2, 0, 255, cv::NORM_MINMAX);
	normData2.convertTo(scharrReturn, CV_8UC1);
	cv::cvtColor(scharrReturn, scharrReturn, cv::COLOR_GRAY2BGR);

	return Image(scharrReturn);
}

Image Image::cannyEdgeDetection() const
{
	cv::Mat gaussianData;
	cv::Mat edges;

	cv::Size filterSize(3, 3);
	cv::GaussianBlur(imageData, gaussianData, filterSize, 0.5, 0.5);

	cv::Canny(gaussianData,
			edges,
			200,
			600,
			3);

	return Image(edges);
}

Image Image::stitch(const Image& target) const
{
	cv::Ptr<cv::FeatureDetector> featureDetectorPointer;
	featureDetectorPointer = cv::ORB::create();

	std::vector<cv::KeyPoint> totalImageKeypointSet;
	std::vector<cv::KeyPoint> currentImageKeypointSet;

	featureDetectorPointer->detect(getData(),   totalImageKeypointSet);
	featureDetectorPointer->detect(target.getData(), currentImageKeypointSet);

	cv::Ptr<cv::DescriptorExtractor> featureExtractorPointer;
	featureExtractorPointer = cv::ORB::create();


    // The descriptors
    cv::Mat totalImageDescriptors;
    cv::Mat currentImageDescriptors;
    // Extract the features corresponding to the keypoints and the pictures
    featureExtractorPointer->compute(getData(),
    		totalImageKeypointSet,
			totalImageDescriptors);
    featureExtractorPointer->compute(target.getData(),
    		currentImageKeypointSet,
			currentImageDescriptors);

    // Match the features
    cv::Ptr<cv::DescriptorMatcher> featureMatcherPointer;
    featureMatcherPointer = cv::DescriptorMatcher::create(
    		"BruteForce-Hamming");

    std::vector<cv::DMatch> matchSetPointer;

    featureMatcherPointer->match(totalImageDescriptors,
    		currentImageDescriptors,
			matchSetPointer);

    double minDistance = std::numeric_limits<double>::max();
    double maxDistance = std::numeric_limits<double>::min();

    for (std::vector<cv::DMatch>::const_iterator ite(matchSetPointer.begin()); ite != matchSetPointer.end(); ++ite)
    {
            if(ite->distance > maxDistance)
            {
            	maxDistance = ite->distance;
            }
            if(ite->distance < minDistance)
            {
            	minDistance = ite->distance;
            }
    }
    std::vector<cv::DMatch> goodMatchSetPointer;
    for (std::vector<cv::DMatch>::const_iterator ite(matchSetPointer.begin()); ite != matchSetPointer.end(); ++ite)
    {
    	std::cout << "test1" << std::endl;
            if (ite->distance < 8 * minDistance)
            {
            	goodMatchSetPointer.push_back(*ite);
            }
    }


    // Store the 2D points in the keypoint lists from the good matches
    std::vector<cv::Point2f> totalImagePointSetPointer;
    std::vector<cv::Point2f> currentImagePointSetPointer;


    // Look at each good match
    for (std::vector<cv::DMatch>::const_iterator ite(goodMatchSetPointer.begin()); ite != goodMatchSetPointer.end(); ++ite)
    {
    	// Get the keypoints from the good match
    	cv::KeyPoint totalImageKeypoint(totalImageKeypointSet[ite->queryIdx]);
        cv::KeyPoint currentImageKeypoint(currentImageKeypointSet[ite->trainIdx]);
        // Add the corresponding 2D points
        totalImagePointSetPointer.push_back(totalImageKeypoint.pt);
        currentImagePointSetPointer.push_back(currentImageKeypoint.pt);
    }

    // Find the Homography Matrix
    cv::Mat homographyMatrix(cv::findHomography(totalImagePointSetPointer,
    		currentImagePointSetPointer, CV_RANSAC));

    // Use the Homography Matrix to warp the images
    cv::Mat output;
    cv::warpPerspective(getData(), output, homographyMatrix, cv::Size(getData().cols + target.getData().cols, getData().rows + 1));
            cv::Mat half(output(cv::Rect(0, 0, target.getData().cols, target.getData().
            rows)));
    target.getData().copyTo(half);
    return Image(output);
}

void Image::show() const
{
	cv::namedWindow( "Image", CV_WINDOW_AUTOSIZE );
	cv::imshow( "Image", imageData );
	cv::waitKey(0);
}

Video::Video(const std::string& filename)
{
	// Check if filename is representing int
	std::istringstream i(filename);
	int x;
	if (i >> x)
	{
		videoCapture = cv::VideoCapture(x);
	}else
	{
		videoCapture = cv::VideoCapture(filename);
	}

	if (!videoCapture.isOpened())
	{
		throw std::runtime_error(std::string("Unable to open video source."));
	}

	fps = videoCapture.get(CV_CAP_PROP_FPS);

	width = videoCapture.get(CV_CAP_PROP_FRAME_WIDTH);
	height = videoCapture.get(CV_CAP_PROP_FRAME_HEIGHT);
}

Video::Video(int num) : Video::Video(std::to_string(num)){}


void Video::play(double scaleFactor, std::function<Image(Image, int* trackValue)> frameCallback, bool trackBar)
{
	cv::Mat currentFrame; // Store the current frame
	cv::Mat outFrame;    // Store the edges detected in the current frame

	double frameTimeS(1.0 / fps);
	int frameTimeMS(round(frameTimeS * 1000.0));

	bool readSuccess(true);
	int key;

	cv::namedWindow("Video", CV_WINDOW_AUTOSIZE);

	int trackBarValue = 30;
	if(trackBar)
	{
		cv::createTrackbar("Value", "Video", &trackBarValue, 100);
	}

	do
	{
		readSuccess = videoCapture.read(currentFrame);
		// The image was read from the video stream
		if (readSuccess)
		{
			outFrame = frameCallback(Image(currentFrame), &trackBarValue).getData();

			cv::Size videoSizeScaled(outFrame.cols * scaleFactor,
					outFrame.rows * scaleFactor);

			// Resize the input if needed
			if (scaleFactor != 1.0)
			{
				cv::resize(outFrame, outFrame, videoSizeScaled);
			}

			// Process the image
			cv::imshow("Video", outFrame);

			key = cv::waitKey(frameTimeMS);
		}
	}
	while(key != 'q' && key != 27 && readSuccess);
}

int GaussianPyramid::nextPowerTwo(int i)
{
	return pow(2, ceil(log2((double)i)));
}

GaussianPyramid::GaussianPyramid(const Image& image, unsigned int levels)
{
	setImage(image, levels);
}

void GaussianPyramid::setImage(const Image& image, unsigned int levels)
{
	cv::Mat padded;
	int newWidth = nextPowerTwo(image.getWidth());
	int newHeight = nextPowerTwo(image.getHeight());

	cv::copyMakeBorder(image.getData(),
			padded,
			0,
			newHeight - image.getHeight(),
			0,
			newWidth - image.getWidth(),
			cv::BORDER_CONSTANT,
			cv::Scalar(128, 128, 128));

	pyramid.push_back(padded.clone());

	for(int i = 0; i < levels; i++)
	{
		std::cout << "blyat" << std::endl;
		// Compute the new image
		cv::pyrDown(padded, padded, cv::Size(padded.cols / 2, padded.rows / 2));
		// Store the new image in the pyramid
		pyramid.push_back(padded.clone());
	}
}

const cv::Mat& GaussianPyramid::getLevel(unsigned int level) const
{
	return pyramid[level];
}

void GaussianPyramid::display() const
{
	// Make sure the pyramid is not empty
	if (pyramid.size())
	{
		// Edge around each level
		int EDGE = 2;

		// Find the image size
		unsigned int width(EDGE);
		unsigned int height(getLevel(0).rows * 1.5 + EDGE * 3.0);

		for (std::vector<cv::Mat>::const_iterator ite(pyramid.begin() + 1); ite != pyramid.end(); ++ite)
		{
			width += ite->cols + EDGE;
		}

		// Make sure the first level can be displayed
		if (width < getLevel(0).cols + EDGE * 2)
		{
			width =  getLevel(0).cols + EDGE * 2;
		}

		// Create an image where to store the pyramid
		cv::Mat pyramid_image(height, width, CV_8UC3);
		int x_offset(EDGE);
		int y_offset(EDGE);

		// Display all the levels
		for (unsigned int i = 0; i < pyramid.size(); ++i)
		{
			// Normalise the current levelbetween 0 and 255
			cv::Mat tmp_image;
			cv::normalize(getLevel(i), tmp_image, 0, 255, cv::NORM_MINMAX, CV_8UC3);

			// Store the current level into the global image
			cv::Mat targetROI = pyramid_image(cv::Rect(x_offset, y_offset, tmp_image.cols, tmp_image.rows));
			tmp_image.copyTo(targetROI);

			// Update the offset
			if (i == 0)
			{
				y_offset += tmp_image.rows + EDGE;
			}
			else
			{
				x_offset += tmp_image.cols + EDGE;
			}
		}

		// Create window
		cv::namedWindow("Pyramid", CV_WINDOW_AUTOSIZE);

		// Show the image
		cv::imshow("Pyramid", pyramid_image);
		cv::waitKey(0);
	}
}
